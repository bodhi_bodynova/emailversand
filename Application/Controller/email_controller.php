<?php
namespace ewald\email\Application\Controller;

use ewald\email\Application\Model\email_order;
use ewald\email\Application\Model\email_user;
use OxidEsales\Eshop\Application\Component\Widget\ArticleBox;
use OxidEsales\Eshop\Application\Model\SmartyRenderer;
use OxidEsales\Eshop\Core\Email;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\EshopCommunity\Application\Model\Article;

class email_controller extends \OxidEsales\Eshop\Application\Controller\FrontendController
{

    protected $_sClass = 'email_controller';


    protected $_sThisTemplate = 'email.tpl';


    protected $_aViewData = null;


    protected $_aDataFM = null;


    protected $_oxorderid = null;



    /**
     * TODO: Sprache laden! kommt vermutlich aus Filemaker?
     * @return null
     */
    public function render(){
        /*if(\OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxorderid') == ""){
            die('Keine Bestellnummer angegeben');
        } */
        //$this->_aDataFM = json_decode('{"Versandart":"DHL", "Zahlart":"Sepa-Lastschrift","Mandatsnummer":"123456789","Iban":"DE987654321","Bic":"1AB2C3"}');//$_POST['data'];
        $this->_aDataFM = json_decode('{"Bestelldatum" : "18.05.2020","Kundennummer" : "294866",
	"LieferEmpfaenger" : "Michaela Röhlk-Hammesfahr\r",
	"LieferLand" : "Deutschland",
	"LieferOrt" : "Solingen",
	"LieferPLZ" : "42697",
	"LieferStrasse" : "Einsteinstr. 5",
	"Oxid" : "ffff12cb2e45078b6c121d33ebb54fc8",
	"Shop" : "endkundenshop",
	"ShopBestellnummer" : "176451"
}');

        if($this->_aDataFM->Oxid == ""){
           die('Keine Bestellnummer angegeben');
        }
        //$this->_oxorderid = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('oxorderid');
        $this->_oxorderid = $this->_aDataFM->Oxid;
        $this->_aViewData['modulePath'] = \OxidEsales\Eshop\Core\Registry::getConfig()->getShopMainUrl().'modules/ewald/email/';
        $this->_aViewData['dataFM'] = $this->_aDataFM;

        parent::render();
        //$this->sendMail();
        return $this->_sThisTemplate;
    }

    public function sendMail() {
        $email = oxNew(Email::class);
        $render = oxNew(SmartyRenderer::class);
        $body = $render->renderTemplate($this->_sThisTemplate,$this->_aViewData);
        $email->setBody($body);

        $email->setSubject('Bodynova Versandbestätigung');
        //$email->setRecipient('c.ewald@bodynova.de');
        $email->setRecipient('christianewald1@gmx.net');
        $email->setFrom('c.ewald@bodynova.de');
        $email->send();
    }

    public function getModulePath(){
        return \OxidEsales\Eshop\Core\Registry::getConfig()->getShopMainUrl().'modules/ewald/email/';
    }

    /**
     * TODO: eventuell einen eigenen Controller für Order
     * @return object beinhaltet alle relevanten Daten aus Ordersicht
     */

    public function getOrder($oxid){

        $oOrder = oxNew(email_order::class);
        $oOrder->load($oxid);
        $oOrder->getBillCountry();
        $oOrder->getDelCountry();
        $oOrder->getOrderArticles(true);
        $oOrder->getBasket();

        return $oOrder;
    }

    public function getProductEm($oxid){
        $oArt = oxNew(Article::class);
        $oArt->load($oxid);
        return $oArt;
    }


    /**
     * lädt UserObjekt
     * @param $sOxid :UserID
     * @return email_user|object
     */
    public function email_getUser($sOxid){
        $oUser = oxNew(email_user::class);
        $oUser->load($sOxid);
        return $oUser;
    }



    /**
     * @param $x: Ordernummer
     * @throws \OxidEsales\Eshop\Core\Exception\DatabaseConnectionException
     */
    public function initEmail($data){
        if(isset($_GET['oxorderid'])){
            $x = $_GET['oxorderid'];
        }
        $this->_aViewData = $data;
        echo '<pre>';
        $oDb = $this->_oDb = \OxidEsales\Eshop\Core\DatabaseProvider::getDb(\OxidEsales\Eshop\Core\DatabaseProvider::FETCH_MODE_ASSOC);
        $QgetOrder = 'SELECT OXID FROM oxorder WHERE OXORDERNR = ?';
        $getOrder = $oDb->getOne($QgetOrder,array($x));
        if($getOrder){
            $this->_oxidOrder = $getOrder;
            $this->loadOrder();
        } else {
            die('Es existiert keine Order mit der Ordernummer');
        }
    }



}