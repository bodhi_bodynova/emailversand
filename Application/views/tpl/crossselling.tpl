[{block name="widget_product_listitem_infogrid_gridpicture"}]
    [{if $product->oxarticles__flagangebotfahne->value == 1}]
    <div class="ribbon" style="left: 5px">
        <a href="[{$_productLink}]"
           title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]"><span>[{$product->oxarticles__angebotfahnetext->value}]</span></a>
    </div>
    [{/if}]
    <div class="picture text-center pictureBox">
        <a href="[{$_productLink}]" title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
            <div class="js-fillcolor">
                <img src="[{$product->getThumbnailUrl()}]"
                     alt="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]"
                     class="img-responsive [{*}]chamelion[{*}]">
                [{oxhasrights ident="SHOWARTICLEPRICE"}]
                [{assign var="oUnitPrice" value=$product->getUnitPrice()}]
                [{if $oUnitPrice}]
                <div class="unit-price-bild">
                        <span id="productPricePerUnit_[{$testid}]" class="pricePerUnit">
                                [{$product->oxarticles__oxunitquantity->value}] [{$product->getUnitName()}] | [{oxprice price=$oUnitPrice currency=$currency}]/[{$product->getUnitName()}]
                        </span>
                </div>
                [{elseif $product->oxarticles__oxweight->value }]
                <div class="unit-price-bild">
                        <span id="productPricePerUnit_[{$testid}]" class="pricePerUnit">
                            <span title="weight">[{oxmultilang ident="WEIGHT"}]</span>
                            <span class="value">[{$product->oxarticles__oxweight->value}] [{oxmultilang ident="KG"}]</span>
                        </span>
                </div>
                [{/if}]
                [{/oxhasrights}]
            </div>
        </a>
    </div>
    [{/block}]

<div class="listDetails">
    [{block name="widget_product_listitem_infogrid_titlebox"}]
    <div class="title" style="margin-left:auto;margin-right: auto; max-width: 90%">
        <a id="[{$testid}]" href="[{$_productLink}]" class="title text-left"
           title="[{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]">
                <span class="text-left">
                    [{$product->oxarticles__oxtitle->value}] [{$product->oxarticles__oxvarselect->value}]
                </span>
        </a>
    </div>
    [{/block}]
    <div class="descblock" style="margin-right: auto;margin-left: auto;max-width: 90%">
        <div class="text-shortdesc">
            <div class="short">
                <span class="text-left">[{$product->oxarticles__oxshortdesc->value|oxtruncate:40:"..."}]</span>
            </div>
            <div class="content text-left">
                [{block name="widget_product_listitem_grid_price"}]
                [{oxhasrights ident="SHOWARTICLEPRICE"}]
                [{assign var="oUnitPrice" value=$product->getUnitPrice()}]
                [{assign var="tprice"     value=$product->getTPrice()}]
                [{assign var="price"      value=$product->getPrice()}]
                [{if $tprice && $tprice->getBruttoPrice() > $price->getBruttoPrice()}]
                <span class="oldPrice text-muted">
                            <del>[{$product->getFTPrice()}] [{$currency->sign}]</del>
                        </span>
                [{/if}]
                [{/oxhasrights}]
                [{/block}]
            </div>
        </div>
    </div>
    <div class="price" style="margin-right: auto;margin-left: auto;max-width: 90%; margin-bottom:0">

        [{block name="widget_product_listitem_grid_price_value"}]
        [{if $product->getFPrice()}]
        <span class="lead text-nowrap">
                                    [{if $product->isRangePrice()}]
	                                    [{oxmultilang ident="PRICE_FROM"}]
	                                    [{if !$product->isParentNotBuyable()}]
		                                    [{$product->getFMinPrice()}]
	                                    [{else}]
		                                    [{$product->getFVarMinPrice()}]
	                                    [{/if}]
                                    [{else}]
	                                    [{if !$product->isParentNotBuyable()}]
		                                    [{$product->getFPrice()}]
	                                    [{else}]
		                                    [{$product->getFVarMinPrice()}]
	                                    [{/if}]
                                    [{/if}]
					[{$currency->sign}]
					[{if $oView->isVatIncluded()}]
						[{if !($product->hasMdVariants() || ($oViewConf->showSelectListsInList() && $product->getSelections(1)) || $product->getVariants())}]*[{/if}]
					[{/if}]
                    </span>
        [{/if}]
        [{/block}]
        <div class="pull-right">
            [{if $product->getFPrice()}]
            [{if $blShowToBasket}]
            [{oxhasrights ident="TOBASKET"}]
            <div class="btn-group">
                <button type="submit" class="btn btn-default no-border hasTooltip"
                        data-placement="bottom"
                        title="[{oxmultilang ident="TO_CART"}]">
                    <i class="fal fa-shopping-cart"></i>
                </button>
                [{if $product->oxarticles__bnflagbestand->value == 2}]
                <button href="[{$_productLink}]" class="btn btn-default no-border">
                    <i class="fa fa-square" style="color:red"></i>
                </button>
                [{elseif $product->oxarticles__bnflagbestand->value == 1}]
                <a href="[{$_productLink}]" class="btn btn-default no-border">

                    <link itemprop="availability" href="http://schema.org/InStock"/>
                    <span class="stockFlag lowStock">
	                                    <i class="fa fa-square" style="color:yellow"></i>
	                                    [{*oxmultilang ident="LOW_STOCK"*}]
	                                </span>
                </a>
                [{elseif $product->oxarticles__bnflagbestand->value == 0}]
                <a href="[{$_productLink}]" class="btn btn-default no-border">
                    <link itemprop="availability " href="http://schema.org/InStock"/>
                    <i class="fa fa-square" style="color:forestgreen"></i>
                </a>
                [{/if}]
            </div>
            [{/oxhasrights}]
            [{else}]
            <button class="btn btn-xs btn-default hasTooltip pull-right no-border"
                    href="[{$_productLink}]">[{*oxmultilang ident="MORE_INFO"*}]
                <i class="fal fa-info" style="font-size: 20px"></i>
            </button>
            [{/if}]
            [{/if}]
        </div>
    </div>
</div>