[{$dataFM|print_r}]
[{assign var ="order" value=$oView->getOrder($dataFM->Oxid)}]

[{assign var="user" value=$oView->email_getUser($order->oxorder__oxuserid->value)}]

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" lang="de"/>
    <title>Bodynova Versandbestätigung</title>
    <link rel="icon" href="[{$modulePath}]out/img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;background-color:lightgrey}
        img {height: auto;}
        p{text-align:justify;word-break:keep-all;}
        .content {width: 100%; max-width: 750px;}
        .header {padding: 40px 30px 20px 30px;}
        .innerpadding {padding: 30px 30px 30px 30px;}
        .borderbottom {border-bottom: 1px solid #f2eeed;}
        .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
        .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
        .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
        .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
        .bodycopy {font-size: 16px; line-height: 22px;}
        .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
        .button a {color: #ffffff; text-decoration: none;}
        .footer {padding: 20px 30px 15px 30px;}
        .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
        .footercopy a {color: #ffffff; text-decoration: underline;}

        @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
            body[yahoo] .hide {display: none!important;}
            body[yahoo] .buttonwrapper {background-color: transparent!important;}
            body[yahoo] .button {padding: 0px!important;}
            body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
            body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
        }
    </style>

</head>

<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:lightgrey">
    <tr>
        <td>

            <table style="background-color:#f6f8f1" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr style="background-color:#60a3b4">
                    <td class="header">
                        <img src="[{$oViewConf->getCurrentHomeDir()}]out/bnflow_child/img/logo_bodynova.png"  border="0" />
                    </td>
                </tr>
                <tr>
                    <td class="innerpadding borderbottom">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td class="h2">
                                    Bodynova Versandbestätigung
                                </td>
                            </tr>
                            <tr>
                                <td class="bodycopy">
                                    <p>Hallo [{$user->oxuser__oxfname->value}] [{$user->oxuser__oxlname->value}],</p>
                                    <p>Ihre Bestellung mit der Bestellnummer [{$order->oxorder__oxordernr->value}] vom [{$order->oxorder__oxorderdate->rawValue|date_format:'%d.%m.%Y'}] ist auf dem Weg.</p>
                                </td>
                            </tr>
                        </table>
                        <table style="margin-top:20px" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr class="hidden-xs ">
                                <td class="bodycopy">
                                    <p><b>Rechnungsadresse</b></p>
                                </td>
                                <td class="bodycopy">
                                    <p><b>Lieferadresse</b></p>
                                </td>
                            </tr>
                            <tr>
                                <td class="bodycopy">
                                    <p>
                                        [{$order->oxorder__oxbillcompany->value}]<br>
                                        [{$order->oxorder__oxbillfname->rawValue}] [{$order->oxorder__oxbilllname->value}]<br>
                                        [{$order->oxorder__oxbillstreet->value}] [{$order->oxorder__oxbillstreetnr->value}]<br>
                                        [{$order->oxorder__oxbillzip->value}] [{$order->oxorder__oxbillcity->value}]<br>
                                        [{$order->oxorder__oxbillcountry->value}]
                                    </p>
                                </td>
                                <td class="bodycopy">
                                    <p>
                                        [{if $order->oxorder__oxdelzip->value ne ""}]
                                            [{$order->oxorder__oxdelcompany->value}]<br>
                                            [{$order->oxorder__oxdelfname->value}] [{$order->oxorder__oxdellname->value}]<br>
                                            [{$order->oxorder__oxdelstreet->value}] [{$order->oxorder__oxdellstreetnr->value}]<br>
                                            [{$order->oxorder__oxdelzip->value}] [{$order->oxorder__oxdellcity->value}]<br>
                                            [{$order->oxorder__oxdelcountry->value}]
                                        [{else}]
                                            [{$order->oxorder__oxbillcompany->value}]<br>
                                            [{$order->oxorder__oxbillfname->rawValue}] [{$order->oxorder__oxbilllname->value}]<br>
                                            [{$order->oxorder__oxbillstreet->value}] [{$order->oxorder__oxbillstreetnr->value}]<br>
                                            [{$order->oxorder__oxbillzip->value}] [{$order->oxorder__oxbillcity->value}]<br>
                                            [{$order->oxorder__oxbillcountry->value}]
                                        [{/if}]
                                    </p>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <tr>
                    <td class="innerpadding borderbottom bodycopy">
                        <p>Kundennummer: [{$user->oxuser__oxcustnr->value}]<br>
                           Auftragsnummer: [{$order->oxorder__oxordernr->value}]<br>
                           Bestelldatum: [{$order->oxorder__oxorderdate->value}]<br>
                           Zahlart: [{$dataFM->Zahlart}]<br>
                           Versandart: [{$dataFM->Versandart}]
                        </p>
                        <p><b>Tracking:</b> Sie können Ihre Bestellung <a href="https://www.google.de" target="_blank">hier</a> verfolgen.</p>
                    </td>
                </tr>
                <tr>
                    <td class="innerpadding borderbottom bodycopy">
                        [{if $dataFM->Zahlart eq "Sepa-Lastschrift"}]
                        <p>Da Sie per SEPA-Lastschrift bezahlen, informieren wir Sie hiermit darüber, dass der Einzug in Höhe von <b>[{$order->oxorder__oxtotalordersum->value|number_format:2:",":"."}]&euro;</b> 3 bis 5 Werktage nach Versand ausgeführt wird. Wir bitten Sie, für ausreichende Kontodeckung zu sorgen.</p>
                        <p>
                            Mandatsnummer: [{$dataFM->Mandatsnummer}]<br>
                            Ihre IBAN: [{$dataFM->Iban}]<br>
                            [{if $order->oxorder__oxbillcountry->value ne 'Deutschland'}]
                            BIC: [{$dataFM->Bic}]
                            [{/if}]
                        </p>
                        [{/if}]
                    </td>
                </tr>
                <tr>
                    <td class="innerpadding borderbottom bodycopy" >
                        <h3><u>Bestelldetails:</u></h3>
                        <table style="width:100%" class="table">
                            <tr>
                                <th class="hidden-xs">Pos.</th>
                                <th>Titel</th>
                                <th>Menge</th>
                                <th><span class="pull-right">Preis</span></th>
                                <th><span class="pull-right">Summe</span></th>
                            </tr>
                            [{assign var="count" value = 1}]
                            [{foreach from=$order->getOrderArticles() item="x"}]
                            <tr>
                                <td class="hidden-xs" style="vertical-align: top; width:10%">
                                    <p>[{$count}]</p>
                                </td>
                                <td style="vertical-align: top; width:50%">
                                    <p  style="width:80%">[{$x->oxorderarticles__oxtitle->value}]</p>
                                </td>
                                <td style="vertical-align: top; width:10%">
                                    <p class="pull-left">[{$x->oxorderarticles__oxamount->value}]</p>
                                </td>
                                <td style="vertical-align: top; width:15%">
                                    <p class="pull-right">[{$x->oxorderarticles__oxbprice->value|number_format:2:",":"."}]&euro;</p>
                                </td>
                                <td style="vertical-align: top; width:20%">
                                    <p class="pull-right">[{$x->oxorderarticles__oxbrutprice->value|number_format:2:",":"."}]&euro;</p>
                                </td>
                            </tr>
                            [{assign var="count" value =$count+1}]
                            [{assign var="product" value=$oView->getProductEm('4f19aa3ad932d3206dd821e4589d7ecf')}]
                            [{/foreach}]
                            <tfoot>
                            [{assign var="vat1" value= $order->oxorder__oxartvatprice1->value}]
                            [{assign var="vat2" value= $order->oxorder__oxartvatprice2->value}]
                            [{assign var="vat" value= $vat1+$vat2}]
                            <tr>
                                <td class="hidden-xs"></td>
                                <td class="hidden-xs"></td>
                                <td colspan="2" class="hidden-xs"><span class="pull-left">Zwischensumme:</span><br>
                                    <span class="pull-left">Versandkosten:</span><br>
                                    [{if $order->oxorder__oxdiscount->value > 0}]<span class="pull-left">Rabatt:</span><br>[{/if}]
                                    <span class="pull-left" style="font-weight:bold">Gesamtsumme:</span><br>
                                    <span class="pull-left" style="white-space: nowrap">Inkl. gesetzlicher MwSt.:</span>
                                </td>
                                <td colspan="3" class="d-xs-block hidden-sm hidden-md hidden-lg"><span class="pull-left">Zwischensumme:</span><br>
                                    <span class="pull-left">Versandkosten:</span><br>
                                    [{if $order->oxorder__oxdiscount->value > 0}]<span class="pull-left">Rabatt:</span><br>[{/if}]
                                    <span class="pull-left" style="font-weight:bold">Gesamtsumme:</span><br>
                                    <span class="pull-left" style="white-space: nowrap">Inkl. gesetzlicher MwSt.:</span>
                                </td>
                                <td><span class="pull-right">[{$order->oxorder__oxtotalbrutsum->value|number_format:2:",":"."}]&euro;</span><br>
                                    <span class="pull-right">[{$order->oxorder__oxdelcost->value|number_format:2:",":"."}]&euro;</span><br>
                                    [{if $order->oxorder__oxdiscount->value > 0}]<span class="pull-right">-[{$order->oxorder__oxdiscount->value|number_format:2:",":"."}]&euro;</span><br>[{/if}]
                                    <span class="pull-right" style="font-weight:bold">[{$order->oxorder__oxtotalordersum->value|number_format:2:",":"."}]&euro;</span><br>
                                    <span class="pull-right">[{$vat|number_format:2:",":"."}]&euro;</span>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td class="innerpadding borderbottom bodycopy">
                        <h3><u>Andere Kunden kauften auch:</u></h3>
                        <div class="item_wrapper" style="height: 490px">
                            <div class="product_miniDisplay"><pre>
                                [{$product|var_dump}]
                                [{*include file="crossselling.tpl"*}]
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="innerpadding bodycopy">
                        <a target="_blank" href="[{$oViewConf->getCurrentHomeDir()}]widerrufsrecht/">Widerrufsrecht</a>
                    </td>
                </tr>
                <tr>
                    <td class="footer" style="background-color:#44525f">
                        <table>
                            <tr>
                                <td style="width:67%;vertical-align: top">
                                    <p style="color:white">
                                        Bodynova GmbH, Aachener Strasse 326-328, 50933 Köln<br>
                                        Geschäftsführer Holger Ebenau<br>
                                        Handelsregister Köln HRB Nr. 30131<br>
                                        Steuernummer: 223/5802/4800<br>
                                        Umsatzsteuer-ID-Nr.: DE 194150608<br>
                                        Es gelten unsere allgemeinen Geschäftsbedingungen.<br>
                                        Gerichtsstand Köln
                                    </p>
                                </td>
                                <td style="width:33%;vertical-align: top">
                                    <p style="color:white" class="pull-right">
                                        <u>Bankverbindung:</u><br>
                                        Kontoinhaber: BODYNOVA GmbH<br>
                                        Kreditinstitut: Postbank Köln<br>
                                        IBAN: DE90370100500284169505<br>
                                        BIC: PBNKDEFF
                                    </p>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script src="[{$modulePath}]out/src/js/jquery.min.js"></script>
</body>
</html>