<?php
$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/ewald/email/';
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = array(
    'id'          => 'email',
    'title'       => [
        'de' => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="Visual CMS">odynova Email-Modul'
    ],
    'description' => [
        'de' => 'Modul zur Generierung von Emails während und nach dem Bestellvorgang'
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '1.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'support@bodynova.de',
    'extend'       => array(
        \OxidEsales\Eshop\Core\Email::class => \ewald\email\Application\Model\email_track::class,
        \OxidEsales\Eshop\Application\Model\Order::class => \ewald\email\Application\Model\email_order::class,
        \OxidEsales\Eshop\Application\Model\User::class => \ewald\email\Application\Model\email_user::class
    ),
    'controllers'       => array(
        'email_controller' => \ewald\email\Application\Controller\email_controller::class
    ),
    'templates'   => array(
        'tracking.tpl' => 'ewald/email/Application/views/tpl/tracking.tpl',
        'email.tpl' => 'ewald/email/Application/views/tpl/email.tpl',
        'crossselling.tpl' => 'ewald/email/Application/views/tpl/crossselling.tpl'
    )
);